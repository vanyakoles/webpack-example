const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: path.resolve(__dirname, 'src/index.js'),
    // {
    //     main: path.resolve(__dirname, 'src/index.js'),
    //     modules: path.resolve(__dirname, 'src/modules.js')
    // },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: "[name].js"
    },
    plugins: [
        new HtmlWebpackPlugin({
            minify:{
                collapseWhitespace: false
            }
        })
    ],
    optimization: {
        minimize: false,
        // splitChunks: {
        //     chunks: 'all',
        // }
    }
}