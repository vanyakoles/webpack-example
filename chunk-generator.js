const { exec } = require("child_process");

exec("cd example", (error, stdout) => {
    console.log('cd example');
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    console.log(`stdout: ${stdout}\n----------------------------`);
});

exec("npx webpack", ((error, stdout) => {
    console.log('webpack');
    if (error) {
        console.log('error:', error);
        return;
    }
    console.log('stdout:', stdout, '\n-------------------------');
  }
))