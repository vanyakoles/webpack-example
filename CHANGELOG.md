# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/vanyakoles/webpack-example/compare/v1.0.0...v1.1.0) (2022-06-08)


### Features

* test add feature ([d98b855](https://gitlab.com/vanyakoles/webpack-example/commit/d98b855e4bd5bdbae1899dc06d835027f626b375))


### Bug Fixes

* test add fix ([d6bea22](https://gitlab.com/vanyakoles/webpack-example/commit/d6bea22ad552f873c987d473f54c20995633748f))

## 1.0.0 (2022-06-08)
