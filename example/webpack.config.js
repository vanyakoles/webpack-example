const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: path.resolve(__dirname, 'src/example-index.js'),
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: "[name].js"
    },
    plugins: [
        new HtmlWebpackPlugin({
            minify:{
                collapseWhitespace: false
            }
        })
    ],
    optimization: {
        minimize: false,
        // splitChunks: {
        //     chunks: 'all',
        // }
    }
}